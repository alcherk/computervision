//
//  CVISThresholdFilterOpenCVTests.m
//  ComputerVision
//
//  Created by Brian Smith on 11/21/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import <XCTest/XCTest.h>

#import "CVISThresholdFilterOpenCV.h"
#import "CVISImageImpOpenCV.h"
#import "CVISPixelFormat.h"


@interface CVISThresholdFilterOpenCVTests : XCTestCase {
    CVISThresholdFilterOpenCV *filter;
}

@end

@implementation CVISThresholdFilterOpenCVTests

- (void)setUp {
    [super setUp];
    filter = [[CVISThresholdFilterOpenCV alloc] init];
}

- (void)tearDown {
    filter = nil;
    [super tearDown];
}

- (void)testSubclass {
    XCTAssert([filter isKindOfClass:[CVISImageFilterOpenCV class]], @"Subclass of CVISImageFilterOpenCV");
}

- (void)testDilateProduct {
    CVISImageImpOpenCV *image = [[CVISImageImpOpenCV alloc] initWithWidth:32 height:16 pixelFormat:[CVISPixelFormat pixelFormatForType_Gray8]];
    filter.source = image;
    
    [filter process];
    
    CVISImageImpOpenCV *destination = filter.destination;
    
    XCTAssert(destination.data != NULL, @"Destination image not null.");
    XCTAssertEqual(image.width, destination.width, @"Image width is equal to destination width");
    XCTAssertEqual(image.height, destination.height, @"Image heigt is equal to destination height");
}

@end
