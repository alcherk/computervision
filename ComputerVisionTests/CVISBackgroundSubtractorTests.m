//
//  CVISBackgroundSubtractorTests.m
//  ComputerVision
//
//  Created by Brian Smith on 12/9/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import <XCTest/XCTest.h>

#import "CVISBackgroundSubtractorTypes.h"
#import "CVISBackgroundSubtractor.h"

@interface CVISBackgroundSubtractorTests : XCTestCase

@end

@implementation CVISBackgroundSubtractorTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testCreateKNN {
    NSDictionary *properties = @{
                                 kCVISPropertyKeyKNNHistory : @100,
                                 kCVISPropertyKeyKNNDistanceThreshold : @300,
                                 kCVISPropertyKeyKNNDetectShadows : @NO
                                 };
    CVISBackgroundSubtractor *subtractor = [CVISBackgroundSubtractor createBackgroundSubtractorKNNWithProperties:properties];
    
    XCTAssertEqualObjects(@100, [subtractor valueForProperty:kCVISPropertyKeyKNNHistory]);
    XCTAssertEqualObjects(@300, [subtractor valueForProperty:kCVISPropertyKeyKNNDistanceThreshold]);
    XCTAssertEqualObjects(@NO, [subtractor valueForProperty:kCVISPropertyKeyKNNDetectShadows]);
}

- (void)testCreateMOG2 {
    NSDictionary *properties = @{
                                 kCVISPropertyKeyMOG2History : @100,
                                 kCVISPropertyKeyMOG2VarThreshold : @8,
                                 kCVISPropertyKeyMOG2DetectShadows : @NO
                                 };
    CVISBackgroundSubtractor *subtractor = [CVISBackgroundSubtractor createBackgroundSubtractorMOG2WithProperties:properties];
    
    XCTAssertEqualObjects(@100, [subtractor valueForProperty:kCVISPropertyKeyMOG2History]);
    XCTAssertEqualObjects(@8, [subtractor valueForProperty:kCVISPropertyKeyMOG2VarThreshold]);
    XCTAssertEqualObjects(@NO, [subtractor valueForProperty:kCVISPropertyKeyMOG2DetectShadows]);
}

- (void)testDefaultPropertyValues {
    NSDictionary *defaultValues = [CVISBackgroundSubtractor defaultValuesForProperties];

    // Test MOG properties
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyMOG2History], @"MOG2 history");
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyMOG2VarThreshold], @"MOG2 distance threshold");
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyMOG2DetectShadows], @"MOG2 detect shadows");

    // Test KNN properties
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyKNNHistory], @"KNN history");
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyKNNDistanceThreshold], @"KNN distance threshold");
    XCTAssertNotNil([defaultValues objectForKey:kCVISPropertyKeyKNNDetectShadows], @"KNN detect shadows");
}

@end
