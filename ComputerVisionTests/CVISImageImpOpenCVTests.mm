//
//  CVISImageImpOpenCVTests.m
//  ComputerVision
//
//  Created by Brian Smith on 11/11/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import <XCTest/XCTest.h>

#import "CVISImageImpOpenCV.h"
#import "CVISImageImpbuffer.h"
#import "CVISPixelFormat.h"

@interface CVISImageImpOpenCVTests : XCTestCase

@end

@implementation CVISImageImpOpenCVTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testInitializeWithCVISImageImpBuffer {
    // verify that a non CVISImageImpOpenCV image creates a cv::Mat with it's buffer.
    CVISImageImpBuffer *sourceImage = [[CVISImageImpBuffer alloc] initWithWidth:16 height:16 pixelFormat:[CVISPixelFormat pixelFormatForType_Gray8]];
    CVISImageImpOpenCV *image = [[CVISImageImpOpenCV alloc] initWithImage:sourceImage];
    
    XCTAssertEqual(image.data, sourceImage.data, @"Data pointers equal");
    XCTAssertEqual(image.width, sourceImage.width, @"Image width equal");
    XCTAssertEqual(image.height, sourceImage.height, @"Image height equal");
    XCTAssertEqual(image.pixelFormat.type, image.pixelFormat.type, @"Image pixel format type equal");
}

- (void)testInitializeWithCVISImageImpOpenCV {
    CVISImageImpOpenCV *sourceImage = [[CVISImageImpOpenCV alloc] initWithWidth:16 height:16 pixelFormat:[CVISPixelFormat pixelFormatForType_Gray8]];
    CVISImageImpOpenCV *imageAlloc = [CVISImageImpOpenCV alloc];
    CVISImageImpOpenCV *imageInit = [imageAlloc initWithImage:sourceImage];
    
    XCTAssertNotEqual(imageAlloc, imageInit, @"Allocated image is not equal initialized object");
    XCTAssertEqual(imageInit, sourceImage, @"Initialized image is equal source image");
}

@end
