//
//  CVISImageImpBuffer.m
//  ObjectMotion
//
//  Created by Brian Smith on 10/27/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISImageImpBuffer.h"
#import "CVISPixelFormat.h"

@interface CVISImageImpBuffer () {
    NSUInteger _width;
    NSUInteger _height;
    NSUInteger _bytesPerRow;
}

@end

@implementation CVISImageImpBuffer

@synthesize pixelFormat = _pixelFormat;

#pragma mark Initialization/Memory
- (id)initWithPixelFormat:(CVISPixelFormat *)format {
    self = [super init];
    if (self == nil) return nil;
    
    _buffer = [NSData data];
    _pixelFormat = format;
    
    return self;
}

- (id)initWithWidth:(NSUInteger)width height:(NSUInteger)height pixelFormat:(CVISPixelFormat *)format {
    self = [super init];
    if (self == nil) return nil;
    
    NSUInteger length = width * height * format.bytesPerPixel;
    void *data = malloc(length);
    _buffer = [NSData dataWithBytes:data length:length];
    _width = width;
    _height = height;
    _bytesPerRow = width * format.bytesPerPixel;
    _pixelFormat = format;
    
    return self;
}

- (id)initWithImageBuffer:(CVImageBufferRef)imageBuffer pixelFormat:(CVISPixelFormat *)pixelFormat {
    
    self = [super init];
    if (self == nil) return nil;
    _pixelFormat = pixelFormat;
    
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    _height = CVPixelBufferGetHeight(imageBuffer);
    _width = CVPixelBufferGetWidth(imageBuffer);
    _bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);

    NSInteger length = _height * _bytesPerRow;
    
    _buffer = [NSData dataWithBytes:CVPixelBufferGetBaseAddress(imageBuffer) length:length];
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);

    return self;
}


#pragma mark Accessors

- (NSUInteger)width {
    return _width;
}

- (NSUInteger)height {
    return _height;
}

- (NSUInteger)bytesPerRow {
    return _bytesPerRow;
}

- (NSUInteger)dataSize {
    return _buffer.length;
}

- (const void *)data {
    return _buffer.bytes;
}

@end
