//
//  CVISImage.m
//  ObjectMotion
//
//  Created by Brian Smith on 9/30/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
     Redistribution and use in source and binary forms, with or without
     modification, are permitted provided that the following conditions are met:
     
     1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
     
     2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
     
     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
     ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
     LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
     CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
     SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
     ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
     POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISImage.h"
#import "CVISPixelFormat.h"
#import "CVISImageImpProtocol.h"
#import "CVISImageImpBuffer.h"
#import "CVISImagePrivate.h"


@interface CVISImage () {
    CGImageRef _cgImage;
    id<CVISImageImpProtocol> _imageImp;
}

@end

@implementation CVISImage

- (id)initWithImageBuffer:(CVImageBufferRef)imageBuffer pixelFormat:(CVISPixelFormat *)pixelFormat {
    self = [super init];
    if (self == nil) return nil;
    
    self.imageImp = [[CVISImageImpBuffer alloc] initWithImageBuffer:imageBuffer pixelFormat:pixelFormat];

    return self;
}

- (id)initWithImplementation:(id<CVISImageImpProtocol>)imageImp {
    self = [super init];
    if (self == nil) return nil;
    
    self.imageImp = imageImp;
    _cgImage = NULL;
    
    return self;
}

- (id)initWithWidth:(NSUInteger)width height:(NSUInteger)height pixelFormat:(CVISPixelFormat *)format {
    self = [super init];
    if (self == nil) return nil;
    
    self.imageImp = [[CVISImageImpBuffer alloc] initWithWidth:width height:height pixelFormat:format];
    
    return self;
}

- (void)dealloc {
    if (_cgImage != NULL) {
        CGImageRelease(_cgImage);
    }
}

- (NSUInteger)width {
    return self.imageImp.width;
}

- (NSUInteger)height {
    return self.imageImp.height;
}

- (const void *)data {
    return self.imageImp.data;
}

- (NSUInteger)dataSize {
    return self.imageImp.dataSize;
}

- (NSUInteger)bytesPerRow
{
    return self.imageImp.bytesPerRow;
}

- (CVISPixelFormat *)pixelFormat {
    return self.imageImp.pixelFormat;
}

- (id<CVISImageImpProtocol>)imageImp {
    return _imageImp;
}

- (void)setImageImp:(id<CVISImageImpProtocol>)imageImp {
    _imageImp = imageImp;
}

- (CGImageRef)cgImage {
    if (_cgImage == NULL) {
        CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, self.data, self.dataSize, NULL);
        
        CGColorSpaceRef colorSpace = NULL;
        CGBitmapInfo    bitmapInfo;
        
        if (self.pixelFormat.type == CVISPixelFormatType_BGR24) {
            colorSpace = CGColorSpaceCreateDeviceRGB();
            bitmapInfo = (CGBitmapInfo)kCGImageAlphaNone;
        } else if (self.imageImp.pixelFormat.type == CVISPixelFormatType_BGRA32) {
            colorSpace = CGColorSpaceCreateDeviceRGB();
            bitmapInfo = kCGImageAlphaFirst | kCGBitmapByteOrder32Little;
        } else if (self.imageImp.pixelFormat.type == CVISPixelFormatType_Gray8) {
            colorSpace = CGColorSpaceCreateDeviceGray();
            bitmapInfo = (CGBitmapInfo)kCGImageAlphaNone;
        } else {
            // TODO: throw and exception to indicate that a colorspace could not be created.
        }
        
        _cgImage = CGImageCreate(self.width, self.height, self.pixelFormat.bitsPerComponent, self.pixelFormat.bitsPerPixel, self.bytesPerRow, colorSpace, bitmapInfo, dataProvider, NULL, NO, kCGRenderingIntentDefault);

        CGColorSpaceRelease(colorSpace);
        CGDataProviderRelease(dataProvider);
    }
    return _cgImage;
}

@end
