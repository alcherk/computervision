//
//  CVISImage.h
//  ObjectMotion
//
//  Created by Brian Smith on 9/30/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
     Redistribution and use in source and binary forms, with or without
     modification, are permitted provided that the following conditions are met:
     
     1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
     
     2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
     
     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
     AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
     ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
     LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
     CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
     SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
     CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
     ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
     POSSIBILITY OF SUCH DAMAGE.
 */

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>

@protocol CVISImageImpProtocol;
@class CVISPixelFormat;

@interface CVISImage : NSObject

// image properties
@property (readonly) NSUInteger         width;
@property (readonly) NSUInteger         height;
@property (readonly) const void         *data;
@property (readonly) NSUInteger         dataSize;
@property (readonly) NSUInteger         bytesPerRow;
@property (readonly) CVISPixelFormat    *pixelFormat;

// conversion properties
@property (readonly) CGImageRef cgImage;

- (id)initWithImageBuffer:(CVImageBufferRef)imageBuffer pixelFormat:(CVISPixelFormat *)pixelFormat;
- (id)initWithWidth:(NSUInteger)width height:(NSUInteger)height pixelFormat:(CVISPixelFormat *)format;

@end
