//
//  CVISFeatureDetectionOpenCV.h
//  ComputerVision
//
//  Created by Brian Smith on 1/30/15.
//  Copyright (c) 2015 Brian Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVISFeatureDetectionOpenCV : NSObject

@property BOOL computeDescriptors;

- (instancetype)initORB;

- (void)detect;

@end
