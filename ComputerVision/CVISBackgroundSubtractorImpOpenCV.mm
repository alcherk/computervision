//
//  CVISBackgroundSubtractorImpOpenCV.m
//  ObjectMotion
//
//  Created by Brian Smith on 10/3/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import <CoreVideo/CoreVideo.h>
#import <opencv2/opencv.hpp>

#import "CVISBackgroundSubtractorImpOpenCV.h"
#import "CVISBackgroundSubtractorPropertiesOpenCVKNN.h"
#import "CVISBackgroundSubtractorPropertiesOpenCVMOG2.h"
#import "CVISImageImpOpenCV.h"
#import "CVISPixelFormat.h"
#import "CVISImage.h"

@interface CVISBackgroundSubtractorImpOpenCV () {
    cv::Ptr<cv::BackgroundSubtractor> backgroundSubtractor;
    id backgroundSubtractorProperties;
}

@end

@implementation CVISBackgroundSubtractorImpOpenCV

@synthesize learningRate;
@synthesize forgroundMask;
@synthesize backgroundImage;

- (instancetype)initKNNWithProperties:(NSDictionary *)properties {
    self = [super init];
    if (self == nil) return nil;
    
    cv::Ptr<cv::BackgroundSubtractorKNN> subtractor = cv::createBackgroundSubtractorKNN();
    backgroundSubtractor = subtractor;
    
    CVISBackgroundSubtractorPropertiesOpenCVKNN *subtractorProperties = [[CVISBackgroundSubtractorPropertiesOpenCVKNN alloc] init];
    subtractorProperties.backgroundSubtractor = subtractor;
    backgroundSubtractorProperties = subtractorProperties;
    
    if (properties != nil) {
        [backgroundSubtractorProperties setValuesForKeysWithDictionary:properties];
    }
    
    self.learningRate = -1;
    
    return self;
}

- (instancetype)initMOG2WithProperties:(NSDictionary *)properties {
    self = [super init];
    if (self == nil) return nil;
    
    cv::Ptr<cv::BackgroundSubtractorMOG2> subtractor = cv::createBackgroundSubtractorMOG2();
    backgroundSubtractor = subtractor;
    
    CVISBackgroundSubtractorPropertiesOpenCVMOG2 *subtractorProperties = [[CVISBackgroundSubtractorPropertiesOpenCVMOG2 alloc] init];
    subtractorProperties.backgroundSubtractor = subtractor;
    backgroundSubtractorProperties = subtractorProperties;
    
    if (properties != nil) {
        [backgroundSubtractorProperties setValuesForKeysWithDictionary:properties];
    }
    
    self.learningRate = -1;
    
    return self;
}

- (instancetype)init
{
    return [self initMOG2WithProperties:nil];
}

- (id)initWithHistory:(NSInteger)history threshold:(double)threshold shadowDetect:(BOOL)shadowDetect
{
    self = [super init];
    if (self == nil) return nil;
    
    backgroundSubtractor = cv::createBackgroundSubtractorMOG2((int)history, threshold, shadowDetect);
    self.learningRate = -1;
    
    return self;
}

- (id<CVISImageImpProtocol>)backgroundImage
{
    CVISImageImpOpenCV *image = [[CVISImageImpOpenCV alloc] initWithPixelFormat:[CVISPixelFormat pixelFormatForType_BGRA32]];
    backgroundImage = image;
    
    cv::Mat bgrImage;
    backgroundSubtractor->getBackgroundImage(bgrImage);
    
    // Add alpha
    cvtColor(bgrImage, *image.imageOpenCV, cv::COLOR_BGR2BGRA);
    
    return backgroundImage;
}

- (void)updateWithFrame:(CVISImage *)frame
{
    size_t height = frame.height;
    size_t stride = frame.bytesPerRow;
    size_t extendedWidth = stride / sizeof( uint32_t ); // each pixel is 4 bytes/32 bits
    
    cv::Mat bgraImage = cv::Mat( (int)height, (int)extendedWidth, CV_8UC4, (void *)frame.data );
    cv::Mat bgrImage;
    cvtColor(bgraImage, bgrImage, cv::COLOR_BGRA2BGR);

    forgroundMask = [[CVISImageImpOpenCV alloc] init];
    CVISImageImpOpenCV *mask = (CVISImageImpOpenCV *)forgroundMask;
    
    backgroundSubtractor->apply(bgrImage, *(mask.imageOpenCV), self.learningRate);
}

- (id)valueForProperty:(NSString *)key {
    return [backgroundSubtractorProperties valueForKey:key];
}

- (void)setValue:(id)value forProperty:(NSString *)key {
    [backgroundSubtractorProperties setValue:value forKey:key];
}

+ (NSDictionary *)defaultValuesForProperties {
    NSMutableDictionary *allDefaults = [NSMutableDictionary dictionary];
    [allDefaults addEntriesFromDictionary:[CVISBackgroundSubtractorPropertiesOpenCVKNN defaultValuesForKeys]];
    [allDefaults addEntriesFromDictionary:[CVISBackgroundSubtractorPropertiesOpenCVMOG2 defaultValuesForKeys]];
    
    return [NSDictionary dictionaryWithDictionary:allDefaults];
}

@end
