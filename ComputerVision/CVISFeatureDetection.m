//
//  CVISFeatureDetection.m
//  ComputerVision
//
//  Created by Brian Smith on 1/30/15.
//  Copyright (c) 2015 Brian Smith. All rights reserved.
//

#import "CVISFeatureDetection.h"
#import "CVISFeatureDetectionOpenCV.h"

@interface CVISFeatureDetection ()
@property CVISFeatureDetectionOpenCV *implementation;
@end

@implementation CVISFeatureDetection

+ (CVISFeatureDetection *)createORB {
    return nil;
}

- (void)detect {
    [self.implementation detect];
}

- (BOOL)computeDescriptors { return self.implementation.computeDescriptors; }
- (void)setComputeDescriptors:(BOOL)computeDescriptors {
    self.implementation.computeDescriptors = computeDescriptors;
}
@end
