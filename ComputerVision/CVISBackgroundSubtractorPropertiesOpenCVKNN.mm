//
//  CVISBackgroundSubtractorPropertiesOpenCVKNN.m
//  ComputerVision
//
//  Created by Brian Smith on 12/9/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISBackgroundSubtractorPropertiesOpenCVKNN.h"
#import "CVISBackgroundSubtractorTypes.h"


using namespace cv;

@implementation CVISBackgroundSubtractorPropertiesOpenCVKNN

-(int)history {
    return self.backgroundSubtractor->getHistory();
}

- (void)setHistory:(int)history {
    self.backgroundSubtractor->setHistory(history);
}

- (double)distanceThreshold {
    return self.backgroundSubtractor->getDist2Threshold();
}

- (void)setDistanceThreshold:(double)distanceThreshold {
    self.backgroundSubtractor->setDist2Threshold(distanceThreshold);
}

- (BOOL)detectShadows {
    return self.backgroundSubtractor->getDetectShadows();
}

- (void)setDetectShadows:(BOOL)detectShadows {
    self.backgroundSubtractor->setDetectShadows(detectShadows);
}


- (id)valueForKey:(NSString *)key {
    NSString *selectorKey = key;
    if ([key hasSuffix:@"KNN"]) {
        selectorKey = [key stringByReplacingOccurrencesOfString:@"KNN" withString:@""];
    }
    return [super valueForKey:selectorKey];
}

- (void)setValue:(id)value forKey:(NSString *)key {
    NSString *selectorKey = key;
    if ([key hasSuffix:@"KNN"]) {
        selectorKey = [key stringByReplacingOccurrencesOfString:@"KNN" withString:@""];
    }
    [super setValue:value forKey:selectorKey];
}

+ (NSDictionary *)defaultValuesForKeys {
    static NSDictionary *defaultValues = nil;
    
    if (defaultValues == nil) {
        // Get the default values from an instance of a BackgrounSubtractorKNN
        Ptr<BackgroundSubtractorKNN> defaultSubtractor = createBackgroundSubtractorKNN();
        defaultValues = @{
                          kCVISPropertyKeyKNNHistory : [NSNumber numberWithInt:defaultSubtractor->getHistory()],
                          kCVISPropertyKeyKNNDistanceThreshold : [NSNumber numberWithDouble:defaultSubtractor->getDist2Threshold()],
                          kCVISPropertyKeyKNNDetectShadows : [NSNumber numberWithBool:defaultSubtractor->getDetectShadows()]
                          };
        
    }
    return defaultValues;
}

@end
