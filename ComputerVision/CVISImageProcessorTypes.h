//
//  CVISImageProcessorTypes.h
//  ComputerVision
//
//  Created by Brian Smith on 11/21/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ComputerVision_CVISImageProcessorTypes_h
#define ComputerVision_CVISImageProcessorTypes_h

typedef NS_ENUM(NSUInteger, CVISThresholdFilterType) {
    CVISThresholdFilterTypeBinary       = 0, // value = value > threshold ? max_value : 0
    CVISThresholdFilterTypeBinaryInv    = 1, // value = value > threshold ? 0 : max_value
    CVISThresholdFilterTypeTrunc        = 2, // value = value > threshold ? threshold : value
    CVISThresholdFilterTypeToZero       = 3, // value = value > threshold ? value : 0
    CVISThresholdFilterTypeToZeroInv    = 4, // value = value > threshold ? 0 : value
    CVISThresholdFilterTypeMask         = 7,
    CVISThresholdFilterTypeOtsu         = 8, // use Otsu algorithm to choose the optimal threshold value
    CVISThresholdFilterTypeTriangle     = 16  // use Triangle algorithm to choose the optimal threshold value
};

typedef NS_ENUM(NSUInteger, CVISContourMode) {
    CVISContourModeExternal             = 0,
    CVISContourModeList                 = 1,
    CVISContourModeConnectedComponent   = 2,
    CVISContourModeTree                 = 3,
};

typedef NS_ENUM(NSUInteger, CVISContourMethod) {
    CVISContourMethodNone       = 0,
    CVISContourMethodSimple     = 1,
};

#endif
