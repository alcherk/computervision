//
//  CVISBackgroundSubtractor.mm
//  ObjectMotion
//
//  Created by Brian Smith on 9/18/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISBackgroundSubtractor.h"
#import "CVISBackgroundSubtractorImpProtocol.h"
#import "CVISBackgroundSubtractorImpOpenCV.h"
#import "CVISImage.h"
#import "CVISImagePrivate.h"
#import "CVISImageImpProtocol.h"

@interface CVISBackgroundSubtractor () {
    id<CVISBackgroundSubtractorImpProtocol> subtractorImp;
}

- (instancetype)initWithImplementation:(id<CVISBackgroundSubtractorImpProtocol>)impl;

@end

@implementation CVISBackgroundSubtractor

+ (CVISBackgroundSubtractor *)createBackgroundSubtractorKNNWithProperties:(NSDictionary *)properties {
    CVISBackgroundSubtractorImpOpenCV *impl = [[CVISBackgroundSubtractorImpOpenCV alloc] initKNNWithProperties:properties];
    return [[CVISBackgroundSubtractor alloc] initWithImplementation:impl];
}

+ (CVISBackgroundSubtractor *)createBackgroundSubtractorMOG2WithProperties:(NSDictionary *)properties {
    CVISBackgroundSubtractorImpOpenCV *impl = [[CVISBackgroundSubtractorImpOpenCV alloc] initMOG2WithProperties:properties];
    return [[CVISBackgroundSubtractor alloc] initWithImplementation:impl];
}

- (instancetype)initWithImplementation:(id<CVISBackgroundSubtractorImpProtocol>)impl {
    self = [super init];
    if (self == nil) return nil;

    subtractorImp = impl;
    
    return self;
}

- (void)updateWithFrame:(CVISImage *)frame
{
    [subtractorImp updateWithFrame:frame];
}

- (double)learningRate
{
    return subtractorImp.learningRate;
}

- (void)setLearningRate:(double)rate
{
    subtractorImp.learningRate = rate;
}

- (CVISImage *)forgroundMask
{
    return [[CVISImage alloc] initWithImplementation:subtractorImp.forgroundMask];
}

- (CVISImage *)backgroundImage
{
    return [[CVISImage alloc] initWithImplementation:subtractorImp.backgroundImage];
}

- (id)valueForProperty:(NSString *)key {
    return [subtractorImp valueForProperty:key];
}

- (void)setVAlue:(id)value forProperty:(NSString *)key {
    return [subtractorImp setValue:value forProperty:key];
}

+ (NSDictionary *)defaultValuesForProperties {
    return [CVISBackgroundSubtractorImpOpenCV defaultValuesForProperties];
}

@end

#pragma mark MOG2 keys
NSString *const kCVISPropertyKeyMOG2History = @"historyMOG2";
NSString *const kCVISPropertyKeyMOG2VarThreshold = @"varThresholdMOG2";
NSString *const kCVISPropertyKeyMOG2DetectShadows = @"detectShadowsMOG2";

#pragma mark KNN properties keys
NSString *const kCVISPropertyKeyKNNHistory = @"historyKNN";
NSString *const kCVISPropertyKeyKNNDistanceThreshold = @"distanceThresholdKNN";
NSString *const kCVISPropertyKeyKNNDetectShadows = @"detectShadowsKNN";
