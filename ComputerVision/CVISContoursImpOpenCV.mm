//
//  CVISContoursImpOpenCV.m
//  ComputerVision
//
//  Created by Brian Smith on 11/21/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISContoursImpOpenCV.h"
#import "CVISImageImpOpenCV.h"
#import "CVISTypes.h"

#import <opencv2/core.hpp>
#import <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

@interface CVISContoursImpOpenCV ()

@property vector<vector<cv::Point>> *contoursOCV;
@property vector<vector<cv::Point>> *contoursPolyOCV;
@property vector<Vec4i> *hierarchyOCV;

- (vector<vector<cv::Point>>*)finalContours;

@end

@implementation CVISContoursImpOpenCV

@synthesize source;
@synthesize contours;
@synthesize hierarchy;
@synthesize offset;
@synthesize mode;
@synthesize method;
@synthesize approximatePolygons;
@synthesize polygonEpsilon;
@synthesize closedPolygons;

- (instancetype)init {
    self = [super init];
    if (self == nil) return nil;
    
    self.approximatePolygons = NO;
    self.polygonEpsilon = 0;
    self.closedPolygons = false;
    
    return self;
}

- (void)dealloc {
    if (_contoursOCV != NULL) delete _contoursOCV;
    if (_contoursPolyOCV != NULL) delete _contoursPolyOCV;
    if (_hierarchyOCV != NULL) delete _hierarchyOCV;
}

- (void)find {
    if (_contoursOCV != NULL) delete _contoursOCV, _contoursOCV = NULL;
    if (_contoursPolyOCV != NULL) delete _contoursPolyOCV, _contoursPolyOCV = NULL;
    if (_hierarchyOCV != NULL) delete _hierarchyOCV, _hierarchyOCV = NULL;
    

    self.contoursOCV = new vector<vector<cv::Point>>();
    self.hierarchyOCV = new vector<Vec4i>();
    
    cv::Point localPoint(self.offset.x, self.offset.y);
    CVISImageImpOpenCV *image = (CVISImageImpOpenCV *)self.source;
    cv::Mat *imageOCV = image.imageOpenCV;
    
    findContours(*imageOCV, *self.contoursOCV, *self.hierarchyOCV, (int)self.mode, (int)self.method);
    
    if (approximatePolygons) {
        self.contoursPolyOCV = new vector<vector<cv::Point>>(self.contoursOCV->size());
        
        for (int i = 0; i < self.contoursOCV->size(); i++) {
            vector<cv::Point> contourOCV = (*self.contoursOCV)[i];
            approxPolyDP(Mat(contourOCV), (*self.contoursPolyOCV)[i], polygonEpsilon, closedPolygons);
        }
    }
}

- (NSArray *)contours {
    vector<vector<cv::Point>> *cppContours = [self finalContours];
    
    NSMutableArray *objcContours = [NSMutableArray arrayWithCapacity:cppContours->size()];
    
    for(std::vector<vector<cv::Point>>::iterator it = cppContours->begin(); it != cppContours->end(); ++it)
    {
        vector<cv::Point> cppContour = *it;
        NSMutableArray *objcContour = [NSMutableArray arrayWithCapacity:cppContour.size()];
        for (vector<cv::Point>::iterator pointIterator = cppContour.begin(); pointIterator != cppContour.end(); ++pointIterator)
        {
            cv::Point p = *pointIterator;
            CVISPoint2D cvisPoint;
            cvisPoint.x = p.x;
            cvisPoint.y = p.y;
            
            [objcContour addObject:[NSValue valueWithBytes:&cvisPoint objCType:@encode(CVISPoint2D)]];
        }
        [objcContours addObject:objcContour];
    }
    
    return objcContours;
}

- (NSArray *)hierarchy {
    NSMutableArray *objcHierarchy = [NSMutableArray arrayWithCapacity:self.hierarchyOCV->size()];
    
    for(std::vector<Vec4i>::iterator it = self.hierarchyOCV->begin(); it != self.hierarchyOCV->end(); ++it)
    {
        Vec4i contourHierarchy = *it;
        [objcHierarchy addObject:[NSValue value:contourHierarchy.val withObjCType:@encode(int[4])]];
    }
    
    return objcHierarchy;
}

- (NSArray *)boundingBoxs {
    vector<vector<cv::Point>> *cppContours = [self finalContours];
    NSMutableArray *boundingBoxs = [NSMutableArray arrayWithCapacity:self.contoursOCV->size()];
    
    for(std::vector<vector<cv::Point>>::iterator it = cppContours->begin(); it != cppContours->end(); ++it)
    {
        cv::Rect boundRect = boundingRect(Mat(*it));
        CVISRect cvisRect;
        cvisRect.x = boundRect.x;
        cvisRect.y = boundRect.y;
        cvisRect.width = boundRect.width;
        cvisRect.height = boundRect.height;
        
        [boundingBoxs addObject:[NSValue valueWithBytes:&cvisRect objCType:@encode(CVISRect)]];
    }
    
    return boundingBoxs;
}

- (vector<vector<cv::Point>>*)finalContours {
    vector<vector<cv::Point>> *cppContours = self.contoursOCV;
    if (self.approximatePolygons) {
        cppContours = self.contoursPolyOCV;
    }

    return cppContours;
}

- (void)setSource:(id<CVISImageImpProtocol>)src {
    source = [[CVISImageImpOpenCV alloc] initWithImage:src];
}

@end
