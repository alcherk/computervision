//
//  CVISContours.m
//  ComputerVision
//
//  Created by Brian Smith on 11/21/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISContours.h"
#import "CVISContoursImpProtocol.h"
#import "CVISImagePrivate.h"

#import "CVISContoursImpOpenCV.h"


@interface CVISContours ()

@property id<CVISContoursImpProtocol> implementation;

@end


@implementation CVISContours

- (instancetype)init {
    self = [super init];
    if (self == nil) return nil;
    
    _implementation = [[CVISContoursImpOpenCV alloc] init];
    
    return self;
}

- (void)find {
    [self.implementation find];
}

- (CVISImage *)source {
    return [[CVISImage alloc] initWithImplementation:self.implementation.source];
}

- (void)setSource:(CVISImage *)source {
    self.implementation.source = source.imageImp;
}

- (NSArray *)contours {
    return self.implementation.contours;
}

- (NSArray *)hierarchy {
    return self.implementation.hierarchy;
}

- (NSArray *)boundingBoxs {
    return self.implementation.boundingBoxs;
}

- (CGPoint)offset {
    return self.implementation.offset;
}

- (void)setOffset:(CGPoint)offset {
    self.implementation.offset = offset;
}

- (CVISContourMode)mode {
    return self.implementation.mode;
}

- (void)setMode:(CVISContourMode)mode {
    self.implementation.mode = mode;
}

- (CVISContourMethod)method {
    return self.implementation.method;
}

- (void)setMethod:(CVISContourMethod)method {
    self.implementation.method = method;
}

- (BOOL)approximatePolygons {
    return self.implementation.approximatePolygons;
}

- (void)setApproximatePolygons:(BOOL)approximatePolygons {
    self.implementation.approximatePolygons = approximatePolygons;
}

- (double)polygonEpsilon {
    return self.implementation.polygonEpsilon;
}

- (void)setPolygonEpsilon:(double)polygonEpsilon {
    self.implementation.polygonEpsilon = polygonEpsilon;
}

- (BOOL)closedPolygons {
    return self.implementation.closedPolygons;
}

- (void)setClosedPolygons:(BOOL)closedPolygons {
    self.implementation.closedPolygons = closedPolygons;
}

@end
