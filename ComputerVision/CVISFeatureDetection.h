//
//  CVISFeatureDetection.h
//  ComputerVision
//
//  Created by Brian Smith on 1/30/15.
//  Copyright (c) 2015 Brian Smith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CVISFeatureDetection : NSObject

@property BOOL computeDescriptors;

+ (CVISFeatureDetection *)createORB;

- (void)detect;

@end
