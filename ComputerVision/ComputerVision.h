//
//  ComputerVision.h
//  ComputerVision
//
//  Created by Brian Smith on 10/28/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/

#import <Foundation/Foundation.h>

//! Project version number for ComputerVision.
FOUNDATION_EXPORT double ComputerVisionVersionNumber;

//! Project version string for ComputerVision.
FOUNDATION_EXPORT const unsigned char ComputerVisionVersionString[];


#import <ComputerVision/CVISTypes.h>

//Imaging
#import <ComputerVision/CVISImage.h>
#import <ComputerVision/CVISPixelFormat.h>
#import <ComputerVision/CVISImageProcessorTypes.h>
#import <ComputerVision/CVISImageFilter.h>
#import <ComputerVision/CVISContours.h>


// Motion processing
#import <ComputerVision/CVISBackgroundSubtractorTypes.h>
#import <ComputerVision/CVISBackgroundSubtractor.h>