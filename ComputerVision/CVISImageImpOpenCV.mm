//
//  CVISImageImpOpenCV.m
//  ObjectMotion
//
//  Created by Brian Smith on 10/3/14.
//  Copyright (c) 2014 Brian Smith. All rights reserved.
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

#import "CVISImageImpOpenCV.h"
#import "CVISPixelFormat.h"


@interface CVISImageImpOpenCV () {
    cv::Mat                     *imageMat;
    id<CVISImageImpProtocol>    sourceImage;
}

@end

@implementation CVISImageImpOpenCV

- (cv::Mat *)imageOpenCV {
    return imageMat;
}

- (NSUInteger)width {
    return imageMat->cols;
}

- (NSUInteger)height {
    return imageMat->rows;
}

- (const void *)data {
    return imageMat->data;
}

- (NSUInteger)dataSize {
    return imageMat->step * imageMat->rows;
}

- (NSUInteger)bytesPerRow {
    return imageMat->step;
}

- (id)init {
    self = [super init];
    if (self == nil) return nil;
    
    imageMat = new cv::Mat();
    _pixelFormat = [CVISPixelFormat pixelFormatForType_Gray8];
    
    return self;
}

- (id)initWithPixelFormat:(CVISPixelFormat *)format
{
    self = [self init];
    if (self == nil) return nil;
    
    _pixelFormat = format;
    
    return self;
}

- (id)initWithWidth:(NSUInteger)width height:(NSUInteger)height pixelFormat:(CVISPixelFormat *)format {
    self = [super init];
    if (self == nil) return nil;
    
    imageMat = new cv::Mat((int)width, (int)height, [[self class] openCVTypeFromCVISPixelFormat:format]);
    _pixelFormat = format;
    
    return self;
}

- (id)initWithImage:(id<CVISImageImpProtocol>)image {
    self = [super init];
    if (self == nil) return nil;
    
    if ([image isKindOfClass:[self class]]) {
        return image;
    }
    
    sourceImage = image;
    imageMat = new cv::Mat((int)image.width, (int)image.height, [[self class] openCVTypeFromCVISPixelFormat:image.pixelFormat], (void *)image.data);
    _pixelFormat = image.pixelFormat;
    
    return self;
}

- (void)dealloc {
    delete imageMat;
    sourceImage = nil;
}

+ (int)openCVTypeFromCVISPixelFormat:(CVISPixelFormat *)format
{
    int type = CV_8UC1;
    if (format.type == CVISPixelFormatType_BGRA32) {
        type = CV_8UC4;
    } else if (format.type == CVISPixelFormatType_BGR24) {
        type = CV_8UC3;
    }
    return type;
}

@end
