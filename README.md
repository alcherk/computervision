# README #

This framework provides an Objective-C wrapper around OpenCV. See the [wiki](https://bitbucket.org/brianksmith/computervision/wiki/Home) for documentation.

##Status##
This project is under development, so there are no releases.